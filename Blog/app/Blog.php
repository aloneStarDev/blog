<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed author
 * @property mixed type
 */
class Blog extends Model
{
    protected $fillable = [
        'name',
        'type',
        'author',
        'subject',
        'body'
    ];
    public function author(){
       return User::Where('id',$this->author)->firstOrFail();
    }
    public function categories(){
        return Category::Where('id',$this->type)->firstOrFail();
    }
}
