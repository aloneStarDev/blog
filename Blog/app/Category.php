<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 */
class Category extends Model
{
    protected $fillable=['name'];
}
