<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function SignIn(){
        return view('auth.login');
    }
    public function SignUp(){
        return View('auth.register');
    }
    public function Login(LoginRequest $request){
        $u = User::Where("email",$request['email'])->first();
        if($u!=null && Hash::check($request['password'],$u->password)){
            auth()->loginUsingId($u->id);
            return view('home')->withErrors(['msg'=>'با موفقیت وارد شدید']);
        }
        return back()->withErrors(['email'=>'نام کاربری یا گذرواژه اشتباه است']);
    }
    public function Register(RegisterRequest $request){
        if($request['password_confirmation'] !== $request['password'])
            return back()->withErrors(['password_confirmation'=>'گذرواژه مطابقت ندارد']);
        $u = new User($request->all());
        $u->setAttribute("password",Hash::make($request['password']));
        if(auth()->user()!=null && auth()->user()->roll == 0)
            $u->setAttribute("roll",$request['roll'] ?? 2);
        else
            $u->setAttribute('roll',2);
        $u->save();
        return back()->withErrors(['msg'=>'ثبت شد']);
    }
    public function Logout(){
        auth()->logout();
        return redirect()->home();
    }
}
