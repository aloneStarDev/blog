<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests\BlogRequest;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create()
    {
        return view('blog.create');
    }

    public function store(BlogRequest $request)
    {
        $post = new Blog($request->all());
        $post->setAttribute('author',auth()->user()->id);
        $post->save();
        return back()->withErrors(['msg'=>'با موفقیت ثبت شد']);
    }

    public function show($id)
    {
        $blog = Blog::Where('id',$id)->firstOrFail();
        $maybeLiked = Blog::Where('type',$blog->type)->take(3)->get();
        return view('blog.index',compact('blog',"maybeLiked"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
