<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::latest()->paginate(20);
        return View('category.index',compact('categories'));
    }
    public function store(Request $request){

        $category = new Category(['name'=>$request->input('name')]);
        $category->save();
        return back()->withErrors(['msg'=>'با موفقیت ثبت شد']);
    }
    public function destroy($id){
        try {
            Category::destroy($id);
        } catch (\Exception $e) {
            return back()->withErrors(['msg'=>'خطایی رخ داد']);
        }
        return back()->withErrors(['msg'=>'حذف شد']);
    }
    public function edit(Request $request){
        $request->validate([
            'name'=>'Required|unique:categories',
            'id'=>'Required'
        ]);

        Category::Where('id',$request->input('id'))->update(["name"=>$request->input('name')]);
        return back()->withErrors(['msg'=>'با موفقیت ویرایش شد']);
    }
}
