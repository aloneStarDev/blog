<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $RecentPost = Blog::latest()->paginate(9);
        return view('welcome',compact('RecentPost'));
    }
}
