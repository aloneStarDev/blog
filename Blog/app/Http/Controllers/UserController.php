<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(20);
        return view('admin.user_list', compact('users'));
    }

    public function destroy($id)
    {
        $u = User::Where('id', $id)->firstOrfail();
        $u->delete();
        return back()->withErrors(['msg' => 'حذف شد']);
    }

    public function edit($id)
    {
        $user = User::Where('id', $id)->firstOrFail();
        return view('admin.user_edit', compact('user'));
    }

    public function update()
    {

        request()->validate([
            'email' => 'required',
            'roll' => 'required',
            'name' => 'required'
        ]);
        $user = User::Where('id', request()->input('id'))->firstOrFail();
        if (request()->has('email') && request()->input('email') != $user->email)
            request()->validate([
                'email' => 'unique:users'
            ]);
        else
            request()->validate(['email' => 'required']);
        if (request()->has('password')){
            if(request()->input('password') == request()->input('password_confirmation'))
                $user->update(['password' => Hash::make(request()->input('password'))]);
            else
                return back()->withErrors(['password'=>'گذرواژه مطابقت ندارد']);
        }
        $user->update(request()->all(['email','roll','name']));
        return redirect()->route('user.list')->withErrors(['msg' => 'ویرایش شد']);
    }
}
