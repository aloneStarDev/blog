<?php

namespace App\Providers;

use App\Blog;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::defaultStringLength(191);

//        view()->composer('layouts.app', function ($view) {
//            $recent = Blog::orderBy('id', 'desc')->take(5)->get();
//            $view->with([
//                'RecentPost' => $recent,
//            ]);
//        });
    }
}
