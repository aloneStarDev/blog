<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * @property mixed roll
 */
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     *
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','roll'
    ];

    public function roll(){
        $access = [ 'master' , 'admin' , 'member' ];
        return $access[$this->roll];
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','roll'
    ];

    public function posts(){
        return $this->$this->hasMany('App\Blog');
    }
}
