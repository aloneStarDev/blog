<?php

use Illuminate\Database\Seeder;
use UserSeeder as uu;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $u = new \App\User([
            "name"=>"master",
            "email"=>"master@domain.com",
            "password"=>\Illuminate\Support\Facades\Hash::make("password"),
            "roll"=>0
        ]);
        $u->save();
    }
}
