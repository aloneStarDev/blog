@extends('layouts.app')

@section('content')
    <h2 class="text-center">Users</h2>
    <div class="container-fluid">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>نام</th>
                <th>ایمیل</th>
                <th>دسترسی</th>
                <th>کنترل</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->roll()}}</td>
                    <td>
                        <div class="btn-group btn-group-sm">
                            <form action="{{route('user.delete',['id'=>$user->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">حذف</button>
                            </form>
                            <a href="{{route('user.edit',['id'=>$user->id])}}" class="btn btn-secondary">ویرایش</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <style>
        .paginator {
            display: table;
            margin: 0 auto;
        }
    </style>

    <div class="paginator">
        {{$users->links()}}
    </div>
@endsection
