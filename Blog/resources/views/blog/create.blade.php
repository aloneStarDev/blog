@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>گزارش</h2>
        <form action="{{route('blog.store')}}" method="POST">
            @method('POST')
            @csrf
            <div class="form-group">
                <label for="name">نام</label>
                <input class="form-control @error('name') is-invalid @enderror" id="name" placeholder="نام گزارش" name="name">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="type">دسته بندی</label>
                <select name="type" id="type" class="form-control @error('type') is-invalid @enderror">
                    @foreach(\App\Category::all() as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                @error('type')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="subject">موضوع</label>
                <input class="form-control @error('subject') is-invalid @enderror" id="subject" placeholder="موضوع گزارش" name="subject">
                @error('subject')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">متن پیام</label>
                <textarea class="form-control @error('body') is-invalid @enderror" rows="5" id="body" name="body"></textarea>
                @error('body')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">ثبت</button>
        </form>
    </div>
@endsection
