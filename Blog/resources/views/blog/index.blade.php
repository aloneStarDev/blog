@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-8">
            <h1 class="text-center">{{$blog->name}}</h1>
            <br/>
            <h3 class="text-right">{{$blog->subject}}</h3>
            <h6 class="text-right">{{$blog->categories()->name}}«</h6>
            <hr/>
            <div class="text-right justify-content-center">
                {!!nl2br($blog->body)!!}
            </div>
        </div>
        <div class="col-lg-4">
                @foreach($maybeLiked as $related)
                    <div class="col-lg-12">
                        <div class="nav-item card">
                            <div class="card-body" style="text-align: right" dir="rtl">
                                <h3>
                                    <div class="text-center">
                                        {{$related->name}}
                                    </div>
                                </h3>
                                <hr/>
                                <h6>
                                    موضوع »
                                </h6>
                                <div class="text-center">
                                    {{$related->subject}}</div>
                                <br/>
                                <h6>
                                    نویسنده »
                                </h6>
                                <div class="text-center">
                                    {{$related->author()->name}}
                                </div>
                                <br/>
                                <h6>
                                    دسته بندی »
                                </h6>
                                <div class="text-center">
                                    {{$related->categories()->name}}
                                </div>

                                <a href="{{route('info',['id'=>$related->id])}}" class="btn btn-info">
                                    ادامه مطلب
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>
    </div>
@endsection
