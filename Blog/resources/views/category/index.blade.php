
@extends('layouts.app')

@section('content')
    <h2 class="text-center">Categories</h2>
    <script>
        function editCategory(id,name) {
            $('#edit_id').val(id);
            $('#edit_name').val(name);
            $('#edit').modal('show');
        }
    </script>
    <style>
        .form-inline .form-control{
            margin-left: 20px;
        }
    </style>
    <div class="modal" id="edit">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">ویرایش کاربر</h4>
                    <button type="button" class="close" data-dismiss="edit" onclick="$('#edit').modal('hide')">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form class="form-inline" action="{{route('category.edit')}}" method="POST">
                        @csrf
                        <input type="hidden" id="edit_id" name="id">
                        <label for="edit_name">نام دسته بندی</label>
                        <input class="form-control" placeholder="Enter category" id="edit_name" name="name">
                        <button type="submit" class="form-control btn btn-success">ثبت</button>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-inline" action="{{route('category.create')}}" method="POST">
            @method('POST')
            @csrf
            <label for="name">نام دسته بندی</label>
            <input class="form-control" placeholder="Enter category" id="name" name="name">
            <button type="submit" class="form-control btn btn-success">ثبت</button>
        </form>
        <table class="table table-hover" id="category-table">
            <thead>
            <tr>
                <th>شناسه</th>
                <th>نام</th>
                <th>کنترل</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td>
                    <td>
                        <div class="btn-group btn-group-sm">
                            <form action="{{route('category.delete',['id'=>$category->id])}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <input type="submit" class="btn btn-danger" value="حذف">
                            </form>
                            <button type="button" onclick="editCategory({{$category->id}},'{{$category->name}}')" class="btn btn-secondary">ویرایش</button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <style>
        .paginator {
            display: table;
            margin: 0 auto;
        }
    </style>

    <div class="paginator">
        {{$categories->links()}}
    </div>
@endsection
