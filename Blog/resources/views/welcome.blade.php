@extends('layouts.app')

@section('content')

    <div class="row">
        @foreach($RecentPost as $recent)
            <div class="col-md-4 ">
                <div class="nav-item card">
                    <div class="card-body" style="text-align: right" dir="rtl">
                        <h3>
                            <div class="text-center">
                                {{$recent->name}}
                            </div>
                        </h3>
                        <hr/>
                        <h6>
                            موضوع »
                        </h6>
                        <div class="text-center">
                            {{$recent->subject}}</div>
                        <br/>
                        <h6>
                            نویسنده »
                        </h6>
                        <div class="text-center">
                            {{$recent->author()->name}}
                        </div>
                        <br/>
                        <h6>
                            دسته بندی »
                        </h6>
                        <div class="text-center">
                            {{$recent->categories()->name}}
                        </div>

                        <a href="{{route('info',['id'=>$recent->id])}}" class="btn btn-info">
                            ادامه مطلب
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <style>
        .paginator {
            display: table;
            margin: 0 auto;
        }
    </style>

    <div class="paginator">
        {{$RecentPost->links()}}
    </div>
@endsection
