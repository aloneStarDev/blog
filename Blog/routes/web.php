<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@index")->name('home');

Route::prefix('/Account')->group(function (){
   Route::get('/Login',"AccountController@SignIn")->name('login');
   Route::post('/Login',"AccountController@Login")->name('login');
   Route::get('/Register',"AccountController@SignUp")->name('register');
   Route::post('/Register',"AccountController@Register")->name('register');
   Route::any('/Logout',"AccountController@Logout")->middleware('can:member')->name('logout');
});
Route::prefix('/Category')->middleware('can:master')->group(function (){
    Route::get('/',"CategoryController@index")->name('category.list');
    Route::delete('/Delete/{id}',"CategoryController@destroy")->name('category.delete');
    Route::post('/Create',"CategoryController@store")->name('category.create');
    Route::post('/Edit',"CategoryController@edit")->name('category.edit');
});
Route::prefix('/Post')->middleware('can:admin')->group(function (){
    Route::get('/',"BlogController@index")->name('blog.list');
    Route::get('/View/{id}',"BlogController@show")->name('info');
    Route::get('/New',"BlogController@create")->name('new');
    Route::post('/Store',"BlogController@store")->name('blog.store');
});
Route::prefix("/User")->middleware('can:master')->group(function (){
    Route::get('/',"UserController@index")->name('user.list');
    Route::delete('/DELETE/{id}',"UserController@destroy")->name('user.delete');
    Route::get('/Edit/{id}',"UserController@edit")->name('user.edit');
    Route::post('/update/',"UserController@update")->name('user.update');
});
